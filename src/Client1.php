<?php

namespace leRisen\MailRuAPI;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Cookie\CookieJar as Cookies;

class Client
{
    const USER_AGENT = 'Mozilla / 5.0(Windows; U; Windows NT 5.1; en - US; rv: 1.9.0.1) Gecko / 2008070208 Firefox / 3.0.1';
    
    private $http;
    
    public function __construct()
    {
        $this->http = new HttpClient([
            'headers' => [
                'Accept' => '*/*',
                'User-Agent' => self::USER_AGENT
            ],
            'cookies' => new Cookies()
        ]);
    }
    
    public function authorization(string $login, string $password, string $domain)
    {
        return $this->http->request('POST', 'https://auth.mail.ru/cgi-bin/auth', [
            'multipart' => [
                [
                    'name' => 'Login',
                    'contents' => $login
                ],
                [
                    'name' => 'Password',
                    'contents' => $password
                ],
                [
                    'name' => 'Domain',
                    'contents' => $domain
                ]
            ]
        ]);
    }
}